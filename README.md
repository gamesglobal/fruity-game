# FruitySlot Game

Thanks for sending over this assessment. Here is the full-stack implementation of the Fruity Slot Game.

## Stack Technologies

Below is a list of all the technologies used within this assessment.

### Back End
- Node.js
- Express
- Typescript
- Crypto (PRNG)

### Front End
- React
- Typescript

### Testing
- Jest
- ts-jest

It's worth noting, this is my first attempt using jest. Couldn't quite get the 10 billion spins to run efficiently... but two tests passed.

## Assessment Points

1. I created a 'gameDefinition.json' file containing the symbols, weights, and payouts.
   - I did remove the "GameDefinitions" top level object here. Apologies if I shouldn't have done this, but it felt redundant. In the real world, I obviously couldn't remove this if it was from an external service.

2. I implemented the backend login in Node/Express. This can be found in the ``'spin'`` function within ``backend/src/controllers/gameController.ts``
   - This uses the JSON object to calculate the result of each spin based on the symbol weights and payouts.
3. I made use of the `crypto` module in from Node.js for the PRNG, mainly to ensure secure and fair random number generation for the spins.
4. I made a... basic... front end to call from an API via a proxy to port 3001. 
   - I'd have loved to make a nicer looking application, however, I sadly didn't have much time. I have included certain features such as cool down, to emulate what a real spin might look like.
   - The `SlotMachine` component allows users to input a bet, initiate a spin, and view the results.
   - I map them to emojis to make it slightly more appealing than text...


