import express from 'express';
import { spin } from '../controllers/gameController';


const router = express.Router();

router.post('/spin', (req, res) => {
    const result = spin();
    res.json(result);
});

export default router;