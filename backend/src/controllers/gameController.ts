interface Symbol {
    name: string;
    weight: number;
    payout: number;
}

import gameDefinition from '../gameDefinition.json';

const symbols: Symbol[] = gameDefinition.Symbols;

import crypto from 'crypto';

export const spin = () => {
    // get the total weight of all the symbols
    // sum of all weights, determines the range of our random numbers
    const totalWeight = symbols.reduce((acc: number, symbol: Symbol) => acc + symbol.weight, 0);

    // Generate a secure random number using the crypto module
    // Random number is between 0 and the total weight
    const randomNum = crypto.randomInt(0, totalWeight);

    // keep track of the sum of weights of what we've seen so far
    let weightSum = 0;

    for (const symbol of symbols) {
        weightSum += symbol.weight;

        // Check if number falls within the weight range
        if (randomNum < weightSum) {
            // If it does, return the current symbol to replicate slot machine.
            // Symbols with a higher weight are more likely to be returned as the random number is more likely to fall within their larger range.
            return symbol;
        }
    }
};