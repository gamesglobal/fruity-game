import express from 'express';

const app = express();
const port = process.env.PORT || 3001;

// import routes
import gameRoutes from './routes/gameRoutes';

app.use('/api', gameRoutes);

app.listen(port, () => {
    console.log(`Fruity Server is running on ${port}`);
});