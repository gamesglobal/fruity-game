import {spin} from '../src/controllers/gameController';

interface SpinResult {
    name: string;
    weight: number;
    payout: number;
}

const calculateStats = (numberOfSpins: number) => {
    let totalPayout = 0;
    let nonBlankSpins = 0;
    let sevenPayout = 0;

    for (let i = 0; i < numberOfSpins; i++) {
        const result: SpinResult = spin();

        totalPayout += result.payout;

        if (result.name !== 'Blank') {
            nonBlankSpins++;
        }

        if (result.name === 'Seven') {
            sevenPayout += result.payout;
        }
    }

    // Calculate the average payout per spin
    const averagePayoutPerSpin = totalPayout / numberOfSpins;

    // Calculate the percentage of non-blank spins
    const percentNonBlankSpins = (nonBlankSpins / numberOfSpins) * 100;

    // Calculate the percentage of the total payout
    const percentPayoutFromSeven = (sevenPayout / totalPayout) * 100;

    return {
        averagePayoutPerSpin,
        percentNonBlankSpins,
        percentPayoutFromSeven
    };
};


describe('Fruity Game Spin Sim', () => {
    const numberOfSpins = 100000000; // Adjust based on performance and precision requirements
    const {averagePayoutPerSpin, percentNonBlankSpins, percentPayoutFromSeven} = calculateStats(numberOfSpins);

    test('Average Payout per Spin', () => {
        const expectedAveragePayout = 0.966851;
        expect(averagePayoutPerSpin).toBeCloseTo(expectedAveragePayout, 2);
    });

    test('% non-Blank', () => {
        const expectedPercentNonBlank = 72.38;
        expect(percentNonBlankSpins).toBeCloseTo(expectedPercentNonBlank, 2);
    });

    test('% of Seven payout', () => {
        const expectedPercentPayoutFromSeven = 8.2873;
        expect(percentPayoutFromSeven).toBeCloseTo(expectedPercentPayoutFromSeven, 2);
    });
});