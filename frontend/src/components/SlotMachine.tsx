import React, {useState} from 'react';
import gameService from '../services/gameService';

interface SpinResult {
    name: string;
    weight: number;
    payout: number;
}

// Map symbols to emojis (for fun)
const symbolToEmoji: Record<string, string> = {
    Blank: '⬜',
    Cherry: '🍒',
    Lemon: '🍋',
    Bell: '🔔',
    Diamond: '💎',
    Seven: '7️⃣'
};

const SlotMachine = () => {
    // create states for actual reactivity and to re-render the component.
    const [result, setResult] = useState<SpinResult | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [showResult, setShowResult] = useState(true); // State for result visibility
    const [bet, setBet] = useState<number>(0); // State to store the bet amount
    const [winnings, setWinnings] = useState<number>(0); // State to store the winnings

    const handleSpin = async () => {
        setIsLoading(true);
        setShowResult(false); // Hide the result when the spin starts
        const spinResult: SpinResult = await gameService.spin();
        setResult(spinResult);

        // Calculate winnings
        const currentWinnings = bet * spinResult.payout;
        setWinnings(currentWinnings);

        // Wait for 3 seconds before allowing another spin and showing the result
        setTimeout(() => {
            setIsLoading(false); // End the cooldown, re-enable the spin button
            setShowResult(true); // Show the result
        }, 3000);
    };

    const handleBetChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        // Update the bet state with the new input value. Make sure it's a number.
        setBet(Number(event.target.value));
    };

    return (
        <div className="slot-machine">
            <h1>Fruity Slot Game</h1>

            <input
                type="number"
                value={bet}
                onChange={handleBetChange}
                placeholder="Place your bet"
            />

            <button onClick={handleSpin} disabled={isLoading || bet <= 0}>
                {isLoading ? 'Spinning...' : 'Spin'}
            </button>

            {showResult && result && (
                <div className="result">
                    {/* Mapping it to an emoji for fun */}
                    <p>You landed on: {symbolToEmoji[result.name]}</p>
                    <p>Payout multiplier: {result.payout}</p>
                    <p>Your winnings: {winnings}</p> {/* Display the winnings */}
                </div>
            )}
        </div>
    );
};

export default SlotMachine;
