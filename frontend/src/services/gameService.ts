const spin = async () => {
    try {
        const response = await fetch('/api/spin', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (!response.ok) {
            const errorText = await response.text();
            throw new Error(`Server returned ${response.status}: ${errorText}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        if (error instanceof Error) {
            console.error("An error occurred during the spin:", error.message);
        } else {
            console.error("An unexpected error occurred during the spin.");
        }
        throw error;
    }
};

export default {
    spin,
};
